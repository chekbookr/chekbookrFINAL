package com.example.annaesmall.chekbookr;

import android.provider.BaseColumns;
/*
ExpenseContract.java

"SQLite Contract":
giving column names to necessary data fields
for an expense transaction
*/

public final class ExpenseContract{
    ExpenseContract () {}

    public static abstract class ExpenseEntry implements BaseColumns{

        public static final String TABLE_NAME = "expenses";

        public static final String COLUMN_EXPENSE_NAME = "expenseName";
        public static final String COLUMN_EXPENSE_AMOUNT = "expenseAmount";
        public static final String COLUMN_EXPENSE_DATE = "expenseDate";
        public static final String COLUMN_EXPENSE_CATEGORY = "expenseCategory";
        public static final String COLUMN_EXPENSE_NECESSARY = "isNecessaryExpense";
        public static final String COLUMN_EXPENSE_RECURRING = "isRecurringExpense";
        public static final String COLUMN_EXPENSE_RECURRING_FREQUENCY = "recurringFrequency";
        public static final String COLUMN_EXPENSE_RECURRING_DATE = "recurringDate";

        //public static final String ="";
    }
}