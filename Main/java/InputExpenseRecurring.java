package com.example.annaesmall.chekbookr;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

/*
InputExpenseRecurring allows the user to set if their expense is recurring or nonrecurring.
If it is recurring, the user can enter the frequency of recurrence.
The entire expense information is then stored in the database.
 */

public class InputExpenseRecurring extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    Button buttonNext;
    Button buttonPrev;

    RadioGroup radioGroupRecurring;
    RadioButton radioButtonRecurring;
    RadioButton radioButtonNonrecurring;

    TextView expenseFrequencyPrompt;
    TextView everyText;
    EditText freq;
    Spinner timePeriodSpinner;

    String category;
    String name;
    String amount;
    String date;
    String necessary;
    String recurring;
    String frequency;
    String time;

    static final String CATEGORY = "category";
    static final String NAME = "name";
    static final String AMOUNT = "amount";
    static final String NECESSARY = "necessary";
    static final String DATE = "date";
    static final String RECURRING = "recurring";
    static final String FREQUENCY = "frequency";
    static final String TIME = "time";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_expense_recurring);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        buttonNext = (Button)findViewById(R.id.expenseNextButton);
        buttonPrev = (Button)findViewById(R.id.expensePreviousButton);

        radioGroupRecurring = (RadioGroup)findViewById(R.id.radioGroupRecurringID);
        radioButtonRecurring = (RadioButton)findViewById(R.id.recurringID);
        radioButtonNonrecurring = (RadioButton)findViewById(R.id.nonrecurringID);

        expenseFrequencyPrompt = (TextView)findViewById(R.id.expenseFrequencyID);
        everyText = (TextView)findViewById(R.id.everyID);
        freq = (EditText)findViewById(R.id.frequencyApartID);
        timePeriodSpinner = (Spinner)findViewById(R.id.timePeriodID);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.frequency_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timePeriodSpinner.setAdapter(adapter);


        Intent i = getIntent();
        category = i.getStringExtra(CATEGORY);
        name = i.getStringExtra(NAME);
        amount = i.getStringExtra(AMOUNT);
        date = i.getStringExtra(DATE);
        necessary = i.getStringExtra(NECESSARY);

        //if nonrecurring is selected, do not show the information to collect
        //if the expense is recurring
        radioGroupRecurring.check(R.id.nonrecurringID);
        expenseFrequencyPrompt.setVisibility(View.INVISIBLE);
        everyText.setVisibility(View.INVISIBLE);
        freq.setVisibility(View.INVISIBLE);
        timePeriodSpinner.setVisibility(View.INVISIBLE);

        radioGroupRecurring.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                //if the expense is nonrecurring, do not show the information
                //to collect if the expense is recurring
                if (checkedId == R.id.nonrecurringID) {
                    buttonNext.setEnabled(true);
                    expenseFrequencyPrompt.setVisibility(View.INVISIBLE);
                    everyText.setVisibility(View.INVISIBLE);
                    freq.setVisibility(View.INVISIBLE);
                    timePeriodSpinner.setVisibility(View.INVISIBLE);

                    //if the expense is recurring, show the prompt to collect
                    //the expense recurring information
                } else if (checkedId == R.id.recurringID) {
                    expenseFrequencyPrompt.setVisibility(View.VISIBLE);
                    everyText.setVisibility(View.VISIBLE);
                    freq.setVisibility(View.VISIBLE);
                    timePeriodSpinner.setVisibility(View.VISIBLE);

                    //checks if the frequency of recurrence has been changed
                    freq.addTextChangedListener(textWatcher);

                    //Checks if freq is "", if so, doesn't allow the next button to be pressed
                    String[] editFields = {freq.getText().toString()};
                    ValidationControl.checkForEmpty(editFields, buttonNext);
                }
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {

                //if the expense is recurring, set the listed frequency and time period
                if (radioButtonRecurring.isChecked()) {
                    recurring = radioButtonRecurring.getText().toString();
                    frequency = freq.getText().toString();
                    time = timePeriodSpinner.getSelectedItem().toString();

                    //if the expense is nonrecurring, set the frequency and time period
                    //as empty
                } else if (radioButtonNonrecurring.isChecked()) {
                    recurring = radioButtonNonrecurring.getText().toString();
                    frequency = "";
                    time = "";
                }

                //store the expense in the Expense database
                TransactionDbHelper myDbHelper = new TransactionDbHelper(getApplicationContext());
                SQLiteDatabase dbw = myDbHelper.getWritableDatabase();
                ContentValues value = new ContentValues();
                ContentValues values = new ContentValues();

                value.put(CategoryContract.CategoryEntry.COLUMN_CATEGORY_NAME, category);
                long newRowId = dbw.insert(
                        CategoryContract.CategoryEntry.TABLE_NAME,  //table name for insert
                        null,  //null is all columns
                        value);  //values for the insert

                //put the values from the screen into the object
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NAME, name);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT, amount);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_DATE, date);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_CATEGORY, category);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NECESSARY, necessary);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING, recurring);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_FREQUENCY, frequency);
                values.put(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_DATE, time);


                if (TransactionControl.inputExpense(values, getApplicationContext()) ){
                    Intent intent = new Intent(getApplicationContext(), DisplayExpense.class);
                    startActivity(intent);
                }
            }
        });

        //if previous is clicked, go back to InputExpenseCategory.class
        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                Intent intent = new Intent(getApplicationContext(), InputExpenseCategory.class);
                startActivity(intent);
            }
        });


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        time = parent.getItemAtPosition(pos).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if ( IntentControl.isInMenu(id) ) {
            Intent intent = IntentControl.getSimpleIntent(getApplicationContext(), id);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //TextWatcher
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String[] editFields = {freq.getText().toString()};
            ValidationControl.checkForEmpty(editFields, buttonNext);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

}
