package com.example.annaesmall.chekbookr;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
HomePage is the main page of the application. The home page tells the user their balance
and easy button shortcuts for viewing expenses and incomes.
 */


public class HomePage extends AppCompatActivity {

    Button buttonViewIncomeList;
    Button buttonViewExpenseList;
    TextView textBalance;
    double balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //creating TextViews and Buttons
        buttonViewIncomeList = (Button) (findViewById(R.id.viewIncomesID));
        buttonViewExpenseList = (Button)(findViewById(R.id.viewExpensesID));

        textBalance = (TextView)(findViewById(R.id.balanceID));

        /*
        Handling button click actions.
         */

        //display income shortcut
        buttonViewIncomeList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {
                Intent intent = new Intent(getApplicationContext(), DisplayIncome.class);
                startActivity(intent);
            }
        });

        //display expense shortcut
        buttonViewExpenseList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2){
                Intent intent = new Intent(getApplicationContext(), DisplayExpense.class);
                startActivity(intent);
            }
        });

        //TransactionControl.clearDb(getApplicationContext());

        //only calculate the balance completely when the app first starts.
        if ( !TransactionControl.isStarted() ){
            TransactionControl.flipStarted();
            //calculate balance
            TransactionControl.calculateBalance(getApplicationContext());
        }

        balance = TransactionControl.getOverallBalance();
        textBalance.setText(String.valueOf(balance));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if ( IntentControl.isInMenu(id) ) {
            startActivity(
                    IntentControl.getSimpleIntent(getApplicationContext(), id)
            );

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}