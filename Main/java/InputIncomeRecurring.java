package com.example.annaesmall.chekbookr;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

/*
InputIncomeRecurring allows the user to set if their income is recurring or nonrecurring.
If it is recurring, the user can enter the frequency of recurrence.
The entire income information is then stored in the database.
 */

public class InputIncomeRecurring extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    Button buttonNext;
    Button buttonPrev;

    RadioGroup radioGroupRecurring;
    RadioButton radioButtonRecurring;
    RadioButton radioButtonNonrecurring;

    TextView incomeFrequencyPrompt;
    TextView everyText;
    EditText freq;
    Spinner timePeriodSpinner;

    String name;
    String amount;
    String recurring;
    String date;
    String frequency;
    String time;

    static final String NAME = "name";
    static final String AMOUNT = "amount";
    static final String DATE = "date";
    static final String RECURRING = "recurring";
    static final String FREQUENCY = "frequency";
    static final String TIME = "time";

    //TextWatcher
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String[] editFields = {freq.getText().toString()};
            ValidationControl.checkForEmpty(editFields, buttonNext);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_income_recurring);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        buttonNext = (Button)findViewById(R.id.incomeNextButton);
        buttonPrev = (Button)findViewById(R.id.incomePreviousButton);

        radioGroupRecurring = (RadioGroup)findViewById(R.id.radioGroupRecurringID);
        radioButtonRecurring = (RadioButton)findViewById(R.id.recurringID);
        radioButtonNonrecurring = (RadioButton)findViewById(R.id.nonrecurringID);


        incomeFrequencyPrompt = (TextView)findViewById(R.id.incomeFrequencyID);
        everyText = (TextView)findViewById(R.id.everyID);
        freq = (EditText)findViewById(R.id.frequencyApartID);
        timePeriodSpinner = (Spinner)findViewById(R.id.timePeriodID);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.frequency_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timePeriodSpinner.setAdapter(adapter);


        Intent i = getIntent();
        name = i.getStringExtra(NAME);
        amount = i.getStringExtra(AMOUNT);
        date = i.getStringExtra(DATE);

        //if nonrecurring is selected, do not show the information to collect
        //if the income is recurring
        radioGroupRecurring.check(R.id.nonrecurringID);
        incomeFrequencyPrompt.setVisibility(View.INVISIBLE);
        everyText.setVisibility(View.INVISIBLE);
        freq.setVisibility(View.INVISIBLE);
        timePeriodSpinner.setVisibility(View.INVISIBLE);

        radioGroupRecurring.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

//if the income is nonrecurring, do not show the information
                //to collect if the income is recurring
                if (checkedId == R.id.nonrecurringID) {
                    buttonNext.setEnabled(true);
                    incomeFrequencyPrompt.setVisibility(View.INVISIBLE);
                    everyText.setVisibility(View.INVISIBLE);
                    freq.setVisibility(View.INVISIBLE);
                    timePeriodSpinner.setVisibility(View.INVISIBLE);

                    //if the income is recurring, show the prompt to collect
                    //the income recurring information
                } else if (checkedId == R.id.recurringID) {
                    incomeFrequencyPrompt.setVisibility(View.VISIBLE);
                    everyText.setVisibility(View.VISIBLE);
                    freq.setVisibility(View.VISIBLE);
                    timePeriodSpinner.setVisibility(View.VISIBLE);
                    freq.addTextChangedListener(textWatcher);

                    //checks if the frequency of recurrence has been changed
                    String[] editFields = {freq.getText().toString()};
                    ValidationControl.checkForEmpty(editFields, buttonNext);
                }
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {

                //if the income is recurring, set the listed frequency and time period
                if (radioButtonRecurring.isChecked()) {
                    recurring = radioButtonRecurring.getText().toString();
                    frequency = freq.getText().toString();
                    time = timePeriodSpinner.getSelectedItem().toString();

                    //if the income is nonrecurring, set the frequency and time period
                    //as empty
                } else if (radioButtonNonrecurring.isChecked()) {
                    recurring = radioButtonNonrecurring.getText().toString();
                    frequency = "";
                    time = "";
                }

                //store the income in the Income database
                //TransactionDbHelper myDbHelper = new TransactionDbHelper(getApplicationContext());
                //SQLiteDatabase dbw = myDbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();



                //put the values from the screen into the object
                values.put(IncomeContract.IncomeEntry.COLUMN_INCOME_NAME, name);
                values.put(IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT, amount);
                values.put(IncomeContract.IncomeEntry.COLUMN_INCOME_DATE, date);
                values.put(IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING, recurring);
                values.put(IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_FREQUENCY, frequency);
                values.put(IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_DATE, time);


                if (TransactionControl.inputIncome(values, getApplicationContext())) {
                    Intent intent = new Intent(getApplicationContext(), DisplayIncome.class);
                    startActivity(intent);
                }
            }
        });

        //if previous is clicked, go back to InputIncome.class
        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                Intent intent = new Intent(getApplicationContext(), InputIncome.class);
                startActivity(intent);
            }
        });


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        time = parent.getItemAtPosition(pos).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if ( IntentControl.isInMenu(id) ) {
            Intent intent = IntentControl.getSimpleIntent(getApplicationContext(), id);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

