package com.example.annaesmall.chekbookr;

import android.content.Context;
import android.content.Intent;

/**
 *
 * class intended to utilize an object oriented approach
 * to changing activities with intents.
 *
 * can handle menu intents, as well as intents that require Intent.PushExtra
 */
public abstract class IntentControl {

    //returns true if its a valid menu item.
    public static boolean isInMenu(int id){
        boolean valid = false;
        switch(id){
            case R.id.homePageID:
                valid = true;
                break;
            case R.id.newIncomeFirstID:
                valid = true;
                break;
            case R.id.newExpenseFirstID:
                valid = true;
                break;
            default:
                break;
        }
        return valid;
    }

    //returns an intent for switching from one activity to another
    //that does not involve getting/putting
    public static Intent getSimpleIntent(Context context, int itemId){

        Intent intent;
        //creating an empty intent
        intent = new Intent();
        switch(itemId) {
            case R.id.homePageID: case R.id.expenseBackHomeButton:
                intent = new Intent(context, HomePage.class);
                break;
            case R.id.newIncomeFirstID:
                intent = new Intent(context, InputIncome.class);
                break;
            case R.id.newExpenseFirstID:
                intent = new Intent(context, InputExpense.class);
                break;
            default:
                break;
        }

        return intent;

    }


}
