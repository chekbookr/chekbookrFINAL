package com.example.annaesmall.chekbookr;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
TransactionDbHelper.class

"DB Helper"
stores strings for SQLi for expense and income table operation
(creation, deletion, updating)
*/

public class TransactionDbHelper extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "transactions.db";

    private static final String TEXT_TYPE = " Text";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_EXPENSE_TABLE =
            "CREATE TABLE " +
                    ExpenseContract.ExpenseEntry.TABLE_NAME + "(" +
                    ExpenseContract.ExpenseEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NAME + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_DATE + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NECESSARY + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_FREQUENCY + TEXT_TYPE + COMMA_SEP +
                    ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_DATE + TEXT_TYPE + " )";

    private static final String SQL_CREATE_INCOME_TABLE =
            "CREATE TABLE " +
                    IncomeContract.IncomeEntry.TABLE_NAME + "(" +
                    IncomeContract.IncomeEntry._ID + " Integer Primary Key" + COMMA_SEP +
                    IncomeContract.IncomeEntry.COLUMN_INCOME_NAME + TEXT_TYPE + COMMA_SEP +
                    IncomeContract.IncomeEntry.COLUMN_INCOME_DATE + TEXT_TYPE + COMMA_SEP +
                    IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT + TEXT_TYPE + COMMA_SEP +
                    IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING + TEXT_TYPE + COMMA_SEP +
                    IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_FREQUENCY + TEXT_TYPE + COMMA_SEP +
                    IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_DATE + TEXT_TYPE + ")";

    private static final String SQL_CREATE_CATEGORY_TABLE =
            "Create Table " +
                    CategoryContract.CategoryEntry.TABLE_NAME + "(" +
                    CategoryContract.CategoryEntry._ID + " Integer Primary Key" + COMMA_SEP +
                    CategoryContract.CategoryEntry.COLUMN_CATEGORY_NAME + TEXT_TYPE + ")";

    private static final String SQL_DELETE_EXPENSE_TABLE = "DROP TABLE IF EXISTS " +
            ExpenseContract.ExpenseEntry.TABLE_NAME;

    private static final String SQL_DELETE_INCOME_TABLE = "DROP TABLE IF EXISTS " +
            IncomeContract.IncomeEntry.TABLE_NAME;

    private static final String SQL_DELETE_CATEGORY_TABLE = "DROP TABLE IF EXISTS " +
            CategoryContract.CategoryEntry.TABLE_NAME;

    public TransactionDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public TransactionDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    //method to create the tables
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_EXPENSE_TABLE);
        db.execSQL(SQL_CREATE_INCOME_TABLE);
        db.execSQL(SQL_CREATE_CATEGORY_TABLE);
    }

    //method for upgrading the sqli db.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(SQL_DELETE_EXPENSE_TABLE);
        db.execSQL(SQL_DELETE_INCOME_TABLE);
        db.execSQL(SQL_DELETE_CATEGORY_TABLE);
        this.onCreate(db);
    }

}