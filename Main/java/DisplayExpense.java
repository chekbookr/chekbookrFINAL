package com.example.annaesmall.chekbookr;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

/*
DisplayExpense displays the list of expenses. Each item displays the expense name, amount,
category, if it is necessary or unnecessary, and if it is recurring or nonrecurring. If it
is recurring, the frequency of recurrence is also displayed.
 */

public class DisplayExpense extends AppCompatActivity {

    TextView emptyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_expense);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //creating listview of expenses
        final ListView listView = (ListView) findViewById(R.id.expense_list);
        emptyTextView = (TextView) findViewById(R.id.emptyExpenseView);

        listView.setEmptyView(emptyTextView);

        int[] to = new int[]{
                R.id.expenseDisplayName,
                R.id.expenseDisplayDate,
                R.id.expenseDisplayCategory,
                R.id.expenseDisplayAmount,
                R.id.expenseDisplayNecessary,
                R.id.expenseDisplayRecurring,
                R.id.expenseDisplayFrequency,
                R.id.expenseDisplayTimePeriod
        };

        //populate the listview with the expenses
        //by calling TransactionControl's simpleCursorAdapter return method for displaying expenses.
        listView.setAdapter(
                TransactionControl.getSimpleCursorDisplay(getApplicationContext(), to, R.layout.row_item_expense, true)
        );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //create a popup menu if an expense is clicked. The options are delete or cancel
                final Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                PopupMenu popup = new PopupMenu(DisplayExpense.this, view);

                popup.getMenuInflater().inflate(R.menu.menu_edit_delete, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        String result;
                        int duration = Toast.LENGTH_SHORT;
                        int id1 = item.getItemId();

                        //if the user selects delete, delete the item from the database and listview
                        //and go to the home page. Also displays a toast that the expense was deleted
                        if (id1 == R.id.deleteID) {

                            String[] selectedExpense = {(String) cursor.getString(cursor.getColumnIndex(ExpenseContract.ExpenseEntry._ID))};

                            TransactionControl.deleteExpense(getApplicationContext(),selectedExpense);

                            result = "Expense Deleted";
                            Toast toast = Toast.makeText(getApplicationContext(), result, duration);
                            toast.show();

                            Intent intent = new Intent(getApplicationContext(), HomePage.class);
                            startActivity(intent);

                            return true;
                        }
                        return true;
                    }
                });

                popup.show();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if (IntentControl.isInMenu(id) ) {
            Intent intent = IntentControl.getSimpleIntent(getApplicationContext(), id);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}