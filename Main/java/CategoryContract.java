package com.example.annaesmall.chekbookr;

import android.provider.BaseColumns;

/*
CategoryContract is the database for the unique categories for expenses
 */
public class CategoryContract {

    CategoryContract () {}

    public static abstract class CategoryEntry implements BaseColumns {

        public static final String TABLE_NAME = "category";
        public static final String COLUMN_CATEGORY_NAME = "categoryName";

        //public static final String ="";
    }
}