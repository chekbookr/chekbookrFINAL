package com.example.annaesmall.chekbookr;

import android.provider.BaseColumns;
/*
IncomeContract.java

"SQLI Contract": 
giving column names to necessary data fields
for an income transaction
*/

public final class IncomeContract{
    IncomeContract () {}

    public static abstract class IncomeEntry implements BaseColumns{

        public static final String TABLE_NAME = "incomes";

        public static final String COLUMN_INCOME_NAME = "incomeName";
        public static final String COLUMN_INCOME_AMOUNT = "incomeAmount";
        public static final String COLUMN_INCOME_DATE = "incomeDate";
        public static final String COLUMN_INCOME_RECURRING = "isRecurringIncome";
        public static final String COLUMN_INCOME_RECURRING_FREQUENCY = "recurringFrequency";
        public static final String COLUMN_INCOME_RECURRING_DATE = "recurringDate";


        //public static final String ="";
    }
}