package com.example.annaesmall.chekbookr;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/*
InputIncome is the first activity in the series of activities to input the income.
This page gets the name of the income, the amount, and the date.

All calendar code is from: http://www.mkyong.com/android/android-date-picker-example/
 */

public class InputIncome extends AppCompatActivity implements View.OnClickListener {

    EditText incomeName;
    EditText incomeAmount;

    private EditText incomeDate;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    Button buttonNext;
    Button buttonHome;

    String amount;
    String name;
    String date;

    static final String NAME = "name";
    static final String AMOUNT = "amount";
    static final String DATE = "date";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_income);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        incomeName = (EditText)(findViewById(R.id.enterIncomeNameID));
        incomeAmount = (EditText)(findViewById(R.id.enterIncomeAmountID));
        incomeDate = (EditText) findViewById(R.id.incomeDateID);
        incomeDate.setInputType(InputType.TYPE_NULL);

        buttonNext = (Button)(findViewById(R.id.incomeNextButton));
        buttonHome = (Button)(findViewById(R.id.incomeBackHomeButton));

        buttonNext.setOnClickListener(this);
        buttonHome.setOnClickListener(this);
        incomeDate.setOnClickListener(this);

        //checks if incomeAmount and incomeDate have been changed
        incomeAmount.addTextChangedListener(textWatcher);
        incomeDate.addTextChangedListener(textWatcher);

        //Checks if incomeAmount is "", if so, doesn't allow the next button to be pressed
        String[] editFields = {incomeAmount.getText().toString(), incomeDate.getText().toString()};
        ValidationControl.checkForEmpty(editFields, buttonNext);

        dateFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                incomeDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View v){
        int id = v.getId();

        //if the date is selected, display it in the editText field
        if(id == incomeDate.getId()) {
            fromDatePickerDialog.show();

            //if the home button is selected, go to the home page
        } else if ( id == buttonHome.getId()){
            Intent intent = new Intent(getApplicationContext(), HomePage.class);
            startActivity(intent);

            //if the next button is selected, go to InputIncomeRecurring.class
        } else if(id == buttonNext.getId()){
            name = incomeName.getText().toString();
            amount = incomeAmount.getText().toString();
            date = incomeDate.getText().toString();

            String incAmt = incomeAmount.getText().toString();
            if ( ValidationControl.validateAmount(getApplicationContext(), incAmt) ) {
                amount = ValidationControl.sanitizeAmount(amount);

                Intent intent = new Intent(getApplicationContext(), InputIncomeRecurring.class);
                intent.putExtra(NAME, name);
                intent.putExtra(AMOUNT, amount);
                intent.putExtra(DATE, date);
                startActivity(intent);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if ( IntentControl.isInMenu(id) ) {
            Intent intent = IntentControl.getSimpleIntent(getApplicationContext(), id);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //TextWatcher
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String[] editFields = {incomeAmount.getText().toString(), incomeDate.getText().toString()};
            ValidationControl.checkForEmpty(editFields, buttonNext);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };
}