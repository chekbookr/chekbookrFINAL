package com.example.annaesmall.chekbookr;

import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

/**
 * ValidationControl.class
 *
 * Validates and sanitizes user input to ensure correct format and progression
 * of inputting an income or expense.
 */
public abstract class ValidationControl {
    private static String INVALID_FORMAT = " IS NOT A DOLLAR AMOUNT!";
    private static String TOO_MANY_DECIMALS = " HAS TOO MANY DECIMALS!";
    private static String TOO_MANY_DIGITS = " HAS TOO MANY DIGITS!";

    public static boolean validateAmount(Context context, String amount) {

        //if the amount the user inputs is NOT a double, the number is invalid.
        try {
            Double.parseDouble(amount);
        } catch (Exception e) {
            //catching the exception and toasting a relevant error message.

            //counter for # of decimal pts.
            int counter = countDecimal(amount);

            //if there are more than 1 decimal points, there are too many decimals!
            if (counter > 1) {
                Toast toast = Toast.makeText(context, "$" + amount + TOO_MANY_DECIMALS, Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(context, "$" + amount + INVALID_FORMAT, Toast.LENGTH_SHORT);
                toast.show();
            }
            return false;
        }

        /**
         * if the number is valid, and has a decimal, check # of digits past the decimal.
         * if the number > 2, the number is invalid and must be changed by user
         * (ie: ##.###)
         */
        int afterDecimal = countAfterDecimal(amount);
        if (afterDecimal > 2) {
            Toast toast = Toast.makeText(context, amount + TOO_MANY_DIGITS, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return true;

    }


    /**
     * If there are no decimal points, the app appends
     * a decimal and two zeros to the end of the string.
     *
     * if there is only 1 decimal,
     * add a "0" to end of amount String
     * --if this number is 2, do nothing
     */

    public static String sanitizeAmount(String amount){
        if (countAfterDecimal(amount) == 0){
            amount = amount.concat(".00");
        }else{
            if ( amount.endsWith(".") ){
                amount = amount.concat("00");
            }else{
                int ad = countAfterDecimal(amount);
                if (ad == 1){
                    amount = amount.concat("0");
                }
            }
        }

        return amount;
    }

    //method that checks for empty editText values ("") and enables the
    //'next' button if there is input in the required fields.
    public static void checkForEmpty(String[] editFields, Button enable){
        boolean empty = false;
        for (String fieldStrings : editFields){
            if ( fieldStrings.equals("") ){
                empty = true;
            }
        }
        enable.setEnabled(!empty);
    }

    //method that counts the # of digits after the decimal point for input
    private static int countAfterDecimal(String amount){
        int afterDecimal = 0;
        if (!(amount.endsWith(".") || countDecimal(amount) == 0)) {
            for (int j = amount.indexOf('.'); j < amount.length() - 1; j++) {
                afterDecimal++;
            }
        }
        return afterDecimal;
    }

    //method to count the number of decimal points in the input string.
    private static int countDecimal(String amount){
        int counter = 0;
        //counts number decimal points in the string.
        for( int i=0; i<amount.length(); i++ ) {
            if( amount.charAt(i) == '.' ) {
                counter++;
            }
        }
        return counter;
    }
}
