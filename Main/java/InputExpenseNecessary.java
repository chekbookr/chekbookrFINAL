package com.example.annaesmall.chekbookr;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.view.*;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class InputExpenseNecessary extends AppCompatActivity {

    Button buttonNext;
    Button buttonPrev;

    RadioGroup radioGroupNecessary;

    RadioButton radioButtonNecessary;
    RadioButton radioButtonUnnecessary;

    String category;
    String name;
    String amount;
    String date;
    String necessary;

    static final String CATEGORY = "category";
    static final String NAME = "name";
    static final String AMOUNT = "amount";
    static final String DATE = "date";
    static final String NECESSARY = "necessary";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_expense_necessary);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        buttonNext = (Button)findViewById(R.id.expenseNextButton);
        buttonPrev = (Button)findViewById(R.id.expensePreviousButton);

        radioGroupNecessary = (RadioGroup)findViewById(R.id.radioGroupNecessaryID);
        radioGroupNecessary.check(R.id.necessaryID);

        radioButtonNecessary = (RadioButton)findViewById(R.id.necessaryID);
        radioButtonUnnecessary = (RadioButton)findViewById(R.id.unnecessaryID);

        Intent i = getIntent();
        category = i.getStringExtra(CATEGORY);
        name = i.getStringExtra(NAME);
        amount = i.getStringExtra(AMOUNT);
        date = i.getStringExtra(DATE);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                Intent intent = new Intent(getApplicationContext(), InputExpenseRecurring.class);
                intent.putExtra(NAME, name);
                intent.putExtra(AMOUNT, amount);
                intent.putExtra(DATE, date);
                intent.putExtra(CATEGORY, category);

                if(radioButtonNecessary.isChecked()){
                    necessary = radioButtonNecessary.getText().toString();
                    intent.putExtra(NECESSARY, necessary);
                } else if(radioButtonUnnecessary.isChecked()){
                    necessary = radioButtonUnnecessary.getText().toString();
                    intent.putExtra(NECESSARY, necessary);
                }
                startActivity(intent);
            }
        });

        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                Intent intent = new Intent(getApplicationContext(), InputExpenseCategory.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if ( IntentControl.isInMenu(id) ) {
            Intent intent = IntentControl.getSimpleIntent(getApplicationContext(), id);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
