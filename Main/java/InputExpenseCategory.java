package com.example.annaesmall.chekbookr;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.view.*;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.List;


/*
InputExpenseCategory allows the user to enter the category their expense falls under.
The user can selected from a list of previous categories or they can type in a new one.
 */
public class InputExpenseCategory extends AppCompatActivity{

    EditText expenseCategory;

    Button buttonNext;
    Button buttonPrev;

    String category;
    String name;
    String amount;
    String date;

    static final String CATEGORY = "category";
    final static String NAME = "name";
    final static String AMOUNT = "amount";
    static final String DATE = "date";

    String existingCategory = "";

    //TextWatcher
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String[] editFields = {expenseCategory.getText().toString()};
            ValidationControl.checkForEmpty(editFields, buttonNext);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_expense_category);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        expenseCategory = (EditText)(findViewById(R.id.enterExpenseCatID));

        buttonNext = (Button)(findViewById(R.id.expenseNextButton));
        buttonPrev = (Button)(findViewById(R.id.expensePreviousButton));

        TransactionDbHelper dbHelper = new TransactionDbHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //
        //out of dataset which columns to use projection

        String[] projection = {
                CategoryContract.CategoryEntry.COLUMN_CATEGORY_NAME,
        };

        String[] bind = {
                CategoryContract.CategoryEntry._ID,
                CategoryContract.CategoryEntry.COLUMN_CATEGORY_NAME,
        };

        //now going to call method to return cursor

        //this cursor now only gives unique categories
        Cursor cursor = db.query(
                true, //yes distinct
                CategoryContract.CategoryEntry.TABLE_NAME, //query category table
                bind, //columns to query
                null, //where clause
                null, //values for where
                CategoryContract.CategoryEntry.COLUMN_CATEGORY_NAME, //group by distinct column
                null, //Having, null says return all rows
                null, //order by
                null  //limit
        );

        //the list items from the layout, will find these in the row_item
        int[] to = new int[]{
                R.id.expenseDisplayCategory
        };

        //set up the Cursor Adapter
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.row_item_category, cursor, projection, to, 0);

        final ListView listView = (ListView)findViewById(R.id.existingCategoryList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //set the category name to the existing category that is selected
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);

                existingCategory = (String) cursor.getString(cursor.getColumnIndex(CategoryContract.CategoryEntry.COLUMN_CATEGORY_NAME));
                if(!existingCategory.equals("")){
                    buttonNext.setEnabled(true);
                }

            }
        });

        //checks if expenseCategory has been changed
        expenseCategory.addTextChangedListener(textWatcher);

        //check if expenseCategory is blank. If so, disable the next button.
        String[] editFields = {expenseCategory.getText().toString()};
        ValidationControl.checkForEmpty(editFields, buttonNext);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v1) {
                //get name and amount from InputExpense.java
                Intent i = getIntent();
                name = i.getStringExtra(NAME);
                amount = i.getStringExtra(AMOUNT);
                date = i.getStringExtra(DATE);

                //put name, amount, and date in a new intent to be sent to InputExpenseNecessary.java
                Intent intent = new Intent(getApplicationContext(), InputExpenseNecessary.class);
                intent.putExtra(NAME, name);
                intent.putExtra(AMOUNT, amount);
                intent.putExtra(DATE, date);

                //if the existingCategory is empty, the category is a typed category to be passed
                //to the intent.
                //if not, put the existing category in the intent
                if (existingCategory.equals("")) {
                    category = expenseCategory.getText().toString();
                    intent.putExtra(CATEGORY, category);
                } else {
                    intent.putExtra(CATEGORY, existingCategory);
                }
                startActivity(intent);
            }
        });

        //go to the previous page if the previous button is selected
        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                Intent intent = new Intent(getApplicationContext(), InputExpense.class);
                startActivity(intent);
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        /*
        check IntentControl if a valid Activity from the overflow menu was selected
        if so, start that activity.
         */
        if ( IntentControl.isInMenu(id) ) {
            Intent intent = IntentControl.getSimpleIntent(getApplicationContext(), id);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
