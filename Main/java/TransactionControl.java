package com.example.annaesmall.chekbookr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

/**
 * TransactionControl.class
 *
 * This class handles the large majority of SQLi DB interactions
 * relevant to the chekbookr application.
 *
 * This class also stores an updating balance, relevant to the user's input of
 * incomes and expenses.
 *
 * This class also contains constant string arrays for reusing db queries
 */
public abstract class TransactionControl {
    //expense column names for db query
    private static final String[] EXP_HEADERS = {
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NAME,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_DATE,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_CATEGORY,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NECESSARY,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_FREQUENCY,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_DATE
    };

    //expense column names for db query (with ID)
    private static final String[] EXP_INFO = {
            ExpenseContract.ExpenseEntry._ID,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NAME,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_DATE,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_CATEGORY,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_NECESSARY,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_FREQUENCY,
            ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_RECURRING_DATE
    };

    //income column names for db query
    private static final String[] INC_HEADERS = {
            IncomeContract.IncomeEntry.COLUMN_INCOME_NAME,
            IncomeContract.IncomeEntry.COLUMN_INCOME_DATE,
            IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT,
            IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING,
            IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_FREQUENCY,
            IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_DATE,

    };

    //income column names for db query (with ID)
    private static final String[] INC_INFO = {
            IncomeContract.IncomeEntry._ID,
            IncomeContract.IncomeEntry.COLUMN_INCOME_NAME,
            IncomeContract.IncomeEntry.COLUMN_INCOME_DATE,
            IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT,
            IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING,
            IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_FREQUENCY,
            IncomeContract.IncomeEntry.COLUMN_INCOME_RECURRING_DATE,
    };

    private static final String EXPENSE_ERROR = "Expense Entry Failed";
    private static final String EXPENSE_SUCCESS = "Expense Entered";
    private static final String INCOME_ERROR = "Income Entry Failed";
    private static final String INCOME_SUCCESS = "Income Entered";

    private static double overallBalance;

    //initial state is that the app is not started.
    private static boolean started = false;
    public TransactionControl(){}

    public static boolean isStarted(){
        return started;
    }

    public static void flipStarted (){
        started = true;
    }

    /**
     * calculateBalance, this method only runs on application startup,
     * generating a total balance from the database.
     */
    public static void calculateBalance(Context context){

        //SQLite db connection
        TransactionDbHelper dbHelper = new TransactionDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //cursor creation
        String[] incomeColumnsNeeded = {IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT};

        //querying the income database for all income values
        Cursor incomeCursor = db.query(
                IncomeContract.IncomeEntry.TABLE_NAME, //requested table (income)
                incomeColumnsNeeded, //requested columns (just amounts)
                null, //requested rows (all)
                null, //group rows by: (nothing)
                null, //filter for groupBy
                null, //orderBy (Doesn't matter for now)
                null //limit # of entries. null = no limit.
        );

        //querying the expense database
        String[] expenseColumnsNeeded = {ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT};
        //querying expense db for all expense values
        Cursor expenseCursor = db.query(
                ExpenseContract.ExpenseEntry.TABLE_NAME,
                expenseColumnsNeeded,
                null,
                null,
                null,
                null,
                null
        );

        //setting the balance to zero for 'recalculation'
        overallBalance = 0;

        //looping through the income cursor, adding to the running balance the amount associated
        //with each entry
        if (incomeCursor != null){
            if (incomeCursor.moveToFirst()){
                while(!incomeCursor.isAfterLast()){
                    //adds balance of the row position the cursor is on.
                    modifyBalance(true, Double.parseDouble(incomeCursor.getString(incomeCursor.getPosition())));
                    incomeCursor.moveToNext();
                }
            }
        }
        incomeCursor.close();

        //looping through the expense cursor, subtracting from the running balance the amount associated
        //with each entry
        if (expenseCursor != null){
            if (expenseCursor.moveToFirst()){
                while(!expenseCursor.isAfterLast()){
                    //subtracts balance of the row position the cursor is on
                    modifyBalance(false, Double.parseDouble(expenseCursor.getString(expenseCursor.getPosition())));
                    expenseCursor.moveToNext();
                }
            }
        }
        expenseCursor.close();
    }

    //modifyBalance is called when a new expense or income is entered,
    //adding or subtracting from the total balance based on the addSub parameter.
    public static void modifyBalance(boolean addSub, double amount){
        //false = expense||subtract. true = income||add.
        if (addSub){
            overallBalance += amount;
        } else{
            overallBalance -= amount;
        }
    }

    //method to simply return the user's chekbookr balance.
    public static double getOverallBalance(){
        return overallBalance;
    }

    //adding an expense to the database, has a already-made ContentValues to
    //input the new income, then modify the balance.
    public static boolean inputExpense(ContentValues values, Context context){
        //SQLite db connection
        TransactionDbHelper dbHelper = new TransactionDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        long insert = db.insert(ExpenseContract.ExpenseEntry.TABLE_NAME, null, values);

        //toasts succesful or unsuccessful db entry insertion.
        if (insert != -1){
            modifyBalance(false, values.getAsDouble(ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT));
            Toast toast = Toast.makeText(context, EXPENSE_SUCCESS, Toast.LENGTH_SHORT);
            toast.show();
            return true;
        } else{
            Toast toast = Toast.makeText(context, EXPENSE_ERROR, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
    }

    //delete expense: simply deletes an expense entry, and modifies balance accordingly.
    public static void deleteExpense(Context context, String[] selectedExpense){
        TransactionDbHelper myDbHelper = new TransactionDbHelper(context);
        SQLiteDatabase dbRead = myDbHelper.getReadableDatabase();

        String[] expenseColumnsNeeded = {ExpenseContract.ExpenseEntry.COLUMN_EXPENSE_AMOUNT};
        //querying expense table, selecting the entry from the DisplayExpense class
        Cursor expenseCursor = dbRead.query(
                ExpenseContract.ExpenseEntry.TABLE_NAME,
                expenseColumnsNeeded,
                selectedExpense[0],
                null,
                null,
                null,
                null
        );
        if (expenseCursor != null){
            if (expenseCursor.moveToFirst()){
                modifyBalance(true, Double.parseDouble(expenseCursor.getString(expenseCursor.getPosition())));
            }
        }
        expenseCursor.close();

        SQLiteDatabase dbWrite = myDbHelper.getWritableDatabase();
        dbWrite.delete(ExpenseContract.ExpenseEntry.TABLE_NAME, "_ID = ?", selectedExpense);
    }

    //deletes an income entry based on selection from DisplayIncome class
    //then updates balance.
    public static void deleteIncome(Context context, String[] selectedIncome){
        TransactionDbHelper myDbHelper = new TransactionDbHelper(context);
        SQLiteDatabase dbRead = myDbHelper.getReadableDatabase();

        String[] incomeColumnsNeeded = {IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT};
        //querying expense db for all expense values
        Cursor incomeCursor = dbRead.query(
                IncomeContract.IncomeEntry.TABLE_NAME,
                incomeColumnsNeeded,
                selectedIncome[0],
                null,
                null,
                null,
                null
        );
        if (incomeCursor != null){
            if (incomeCursor.moveToFirst()){
                modifyBalance(false, Double.parseDouble(incomeCursor.getString(incomeCursor.getPosition())));
            }
        }
        incomeCursor.close();

        SQLiteDatabase dbWrite = myDbHelper.getWritableDatabase();
        dbWrite.delete(IncomeContract.IncomeEntry.TABLE_NAME, "_ID = ?", selectedIncome);
    }

    //enters an income to the database and updates balance.
    public static boolean inputIncome(ContentValues values, Context context){
        //SQLite db connection
        TransactionDbHelper dbHelper = new TransactionDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        long insert = db.insert(IncomeContract.IncomeEntry.TABLE_NAME, null, values);

        //toasts succesful or unsuccessful db entry insertion.
        if (insert != -1){
            modifyBalance(true, values.getAsDouble(IncomeContract.IncomeEntry.COLUMN_INCOME_AMOUNT));
            Toast toast = Toast.makeText(context, INCOME_SUCCESS, Toast.LENGTH_SHORT);
            toast.show();
            return true;
        } else{
            Toast toast = Toast.makeText(context, INCOME_ERROR, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
    }

    //method that returns a simpleCursorAdapter for the Display classes.
    public static SimpleCursorAdapter getSimpleCursorDisplay(Context context,int[] to, int row_item, boolean EorI){
        //sqlite connection
        TransactionDbHelper dbHelper = new TransactionDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor;
        SimpleCursorAdapter c;
        if (EorI){
            cursor = db.query(
                    ExpenseContract.ExpenseEntry.TABLE_NAME,
                    EXP_INFO,
                    null,
                    null,
                    null,
                    null,
                    null
            );
            c = new SimpleCursorAdapter(
                    context,
                    row_item,
                    cursor,
                    EXP_HEADERS,
                    to,
                    0
            );
        }else{
            cursor = db.query(
                    IncomeContract.IncomeEntry.TABLE_NAME,
                    INC_INFO,
                    null,
                    null,
                    null,
                    null,
                    null
            );
            c = new SimpleCursorAdapter(
                    context,
                    row_item,
                    cursor,
                    INC_HEADERS,
                    to,
                    0
            );
        }
        return c;
    }

    public static void clearDb(Context context){
        TransactionDbHelper dbHelper = new TransactionDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        dbHelper.onUpgrade(db, 1, 1);
    }

}
